$(document).ready(function(){ 

// radio-select
$('.check input').focus(function () {
    $(this).parent().addClass('selected');
}).blur(function () {
    $(this).parent().removeClass('selected');
});

// scroll --> back to top
    $(window).scroll(function(){ 
        if ($(this).scrollTop() > 100) { 
            $('#scroll').fadeIn(); 
        } else { 
            $('#scroll').fadeOut(); 
        } 
    }); 
    $('#scroll').click(function(){ 
        $("html, body").animate({ scrollTop: 0 }, 200); 
        return false; 
    }); 

// modal 
$('#modal').on('shown.bs.modal', function () {
    $('#myInput').trigger('focus')
  })
});